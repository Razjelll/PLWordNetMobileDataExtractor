from db_helper import *
from configuration.configuration import Configuration

def main():
    configuration = Configuration('/home/roman/PycharmProjects/PLWordnetMySQLtoSQLite/config/config.conf')
    print(configuration.get_columns('domain'))
    export = configuration.get_export_dict()
    for name, tables in export.items():
        print(name)
        print(tables)

    helper = DbHelper()
    data = {}
    for table, columns in configuration.get_tables().items():
        result = helper.get(columns, table)
        data[table] = result

    to_write = {}
    for name, tables in export.items():
        if len(tables) !=1:
            primary_table = tables[0]
            for i in range(1, len(tables)):
                table = tables[i]
                for id, values in data[table].items():
                    for x in range(1, len(values)):
                        data[primary_table][id].append(values[x])
        to_write[name] = data[tables[0]]

    file = open("/home/roman/result.txt", 'w+')
    for table, dict in to_write.items():
        file.write("!" + table + '\n')
        for id, values in dict.items():
            for value in values:
                file.write(str(value))
                file.write("\t")
            file.write("\n")
    file.close()


main()