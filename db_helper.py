import sqlalchemy
from sqlalchemy import orm
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import label


from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base

class DbHelper:

    def __init__(self):
        engine = self.create_alchemy_engine()
        self.__session = self.create_alchemy_session(engine)

    def create_alchemy_engine(self):
        # TODO przerzucić to do jakiego pliku konfiguracyjnego
        settings = {'drivername': 'mysql', 'host': '127.0.0.1', 'port': '3306', 'database': 'wordnet',
                    'username': 'root', 'password': 'root', 'query': {'charset': 'utf8mb4'}}
        url = sqlalchemy.engine.url.URL(**settings)
        engine = sqlalchemy.create_engine(url, echo=False)
        return engine

    def create_alchemy_session(self, engine):
        session = sqlalchemy.orm.sessionmaker(bind=engine)
        return session()

    def get(self, columns, table):
        query = self.__get_select_query(columns, table)
        query_result =  self.__session.execute(query)
        result = {}
        for row in query_result:
            element = []
            for field in row:
                element.append(field)
            result[element[0]] = element
        return result

    def __get_select_query(self, columns, table):
        return "SELECT X FROM Y".replace('X', columns).replace('Y', table)


Base = declarative_base()

class LocalisedStrings(Base):

    __tablename__ = 'application_localised_string'

    id = Column('id', Integer, primary_key=True)
    value = Column(String(255))
    language = Column(String(255))

class Dictionaries(Base):

    __tablename__ = 'dictionaries'
    # TODO przyjrzeć się
    id = Column(Integer, primary_key = True)
    name_id = Column(Integer)
    value = Column(Integer)
    dtype = Column(String(255))

class Domain(Base):

    __tablename__ = 'domain'

    id = Column(Integer, primary_key=True)
    description_id = Column(Integer)
    name_id = Column(Integer)

class Lexicon(Base):

    __tablename__ = 'lexicon'

    id = Column(Integer, primary_key = True)
    language = Column('language_name',String(255))
    version = Column('lexicon_version',String(255))

class RelationType(Base):

    __tablename__ = 'relation_type'

    id = Column(Integer, primary_key = True)
    name = Column('name_id',Integer)
    short_display = Column('short_display_text_id',Integer)
    display = Column('display_text_id', Integer)
    parent = Column('parent_relation_type_id', Integer)
    position = Column('node_position', String(255))

class Synset(Base):

    __tablename__ = 'synset'

    id = Column(Integer, primary_key = True)
    lexicon = Column('lexicon_id', Integer)
    split = Column(Integer)

class SynsetAttributes(Base):

    __tablename__ = "synset_attributes"

    id = Column('synset_id', Integer, primary_key=True)
    definition = Column(String(255))

class SynsetExample(Base):

    __tablename__ = 'synset_examples'

    id = Column('synset_attributes_id', Integer, primary_key=True)
    example = Column(String(255))

class SynsetRelation(Base):

    __tablename__ = 'synset_relation'

    id = Column(Integer, primary_key=True)
    source = Column('child_synset_id', Integer)
    target = Column('parent_synset_id', Integer)
    relation = Column('synset_relation_type_id', Integer)

class Word(Base):

    __tablename__ = 'word'

    id = Column(Integer, primary_key=True)
    word = Column(String(255))

class PartOfSpeech(Base):

    __tablename__ = 'part_of_speech'

    id = Column(Integer, primary_key=True)
    name = Column('name_id', Integer)

class Sense(Base):

    __tablename__ = 'sense'

    id = Column(Integer, primary_key=True)
    word = Column('word_id', Integer)
    variant = Column(Integer)
    part_of_speech = Column('part_of_speech_id', Integer)
    domain = Column('domain_id', Integer)
    lexicon = Column('lexicon_id', Integer)
    synset = Column('synset_id', Integer)
    synset_position = Column(Integer)



