from configparser import ConfigParser

class Configuration:

    def __init__(self, configuration_filename):
        TABLES = 'tables'
        EXPORT = 'export'
        parser = ConfigParser()
        parser.read(configuration_filename)
        self.__columns = dict(parser.items(TABLES))
        config_export = dict(parser.items(EXPORT))
        self.__export = {}
        for name, tables in config_export.items():
            tables_array = tables.strip().replace(' ', '').split('+')
            self.__export[name] = tables_array


    def get_columns(self, table):
        return self.__columns[table]

    def get_tables(self):
        return self.__columns

    def get_export_dict(self):
        return self.__export